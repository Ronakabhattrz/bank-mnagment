Rails.application.routes.draw do

  resources :depositemoneys

  resources :deposites

  get 'profile/index'

  get 'home/index'

  devise_for :users
      root to: "home#index"

end
