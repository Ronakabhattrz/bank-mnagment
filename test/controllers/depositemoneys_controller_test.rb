require 'test_helper'

class DepositemoneysControllerTest < ActionController::TestCase
  setup do
    @depositemoney = depositemoneys(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:depositemoneys)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create depositemoney" do
    assert_difference('Depositemoney.count') do
      post :create, depositemoney: { balance: @depositemoney.balance }
    end

    assert_redirected_to depositemoney_path(assigns(:depositemoney))
  end

  test "should show depositemoney" do
    get :show, id: @depositemoney
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @depositemoney
    assert_response :success
  end

  test "should update depositemoney" do
    patch :update, id: @depositemoney, depositemoney: { balance: @depositemoney.balance }
    assert_redirected_to depositemoney_path(assigns(:depositemoney))
  end

  test "should destroy depositemoney" do
    assert_difference('Depositemoney.count', -1) do
      delete :destroy, id: @depositemoney
    end

    assert_redirected_to depositemoneys_path
  end
end
