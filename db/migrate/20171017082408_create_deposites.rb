class CreateDeposites < ActiveRecord::Migration
  def change
    create_table :deposites do |t|
      t.integer :user_id
      t.integer :deposite_amt

      t.timestamps
    end
  end
end
