class CreateDepositemoneys < ActiveRecord::Migration
  def change
    create_table :depositemoneys do |t|
      t.integer :balance

      t.timestamps
    end
  end
end
