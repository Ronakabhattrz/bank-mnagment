class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  has_many :deposites

      validates :name, presence: true
      validates :address, presence: true
      validates :balance, presence: true
      validates :address, presence: true

end


