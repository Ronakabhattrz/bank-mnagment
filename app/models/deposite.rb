class Deposite < ActiveRecord::Base
	  belongs_to :user
  validates :user_id, presence: true
  validates :deposite_amt, presence: true
  # validates :deposite_amt, format: { with: /\A\d+\z/, message: "Integer only. No sign allowed." }
  validates :deposite_amt, numericality: { greater_than: 0 }



	def user_balance
		user.balance
	end
end





