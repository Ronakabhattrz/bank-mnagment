class Depositemoney < ActiveRecord::Base
    # validates :balance, :numericality => {:only_integer => true}
    # validates :balance, presence: true
      validates :balance, presence: true
             # validates :balance, :inclusion => {:in => [1,2]}
      # validates :balance, format: { with: /\A\d+\z/, message: "Integer only. No sign allowed." }
        validates :balance, numericality: { greater_than: 0 }
end
