json.extract! depositemoney, :id, :balance, :created_at, :updated_at
json.url depositemoney_url(depositemoney, format: :json)
