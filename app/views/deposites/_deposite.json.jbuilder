json.extract! deposite, :id, :user_id, :deposite_amt, :created_at, :updated_at
json.url deposite_url(deposite, format: :json)
