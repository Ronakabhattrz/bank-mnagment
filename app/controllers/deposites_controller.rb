class DepositesController < ApplicationController
  before_action :set_deposite, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @deposites = Deposite.all
    respond_with(@deposites)
  end

  def show
    respond_with(@deposite)
  end

  def new
    @users = User.all
    @deposite = Deposite.new
    respond_with(@deposite)
  end

  def edit
  end

def create
if deposite_params.present?
    @deposite = Deposite.new(deposite_params)
    @deposite.save
    
    @user = User.find deposite_params[:user_id]
    @balance = @user.balance.to_i
    @deposite_money = deposite_params[:deposite_amt].to_i

          @user = User.find deposite_params[:user_id]
          @balance = @user.balance.to_i
          @deposite_money = deposite_params[:deposite_amt].to_i
         
          total_deposite = @deposite_money + @balance

          # abort total_deposite.inspect
          if total_deposite > 0
              abc = @user.update_attribute(:balance, total_deposite)
          end
          @user = User.find current_user.id
          deduct_bal = @balance - @deposite_money
          if deduct_bal > 0
              xyz = @user.update_attribute(:balance, deduct_bal)
          end          
          
      redirect_to profile_index_path
end      
end

  def update
    @deposite.update(deposite_params)
    respond_with(@deposite)
  end

  def destroy
    @deposite.destroy
    respond_with(@deposite)
  end

  private
    def set_deposite
      @deposite = Deposite.find(params[:id])
    end

    def deposite_params
      params.require(:deposite).permit(:user_id, :deposite_amt, :u_id)
    end
end
