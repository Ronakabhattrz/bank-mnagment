class DepositemoneysController < ApplicationController
  before_action :set_depositemoney, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @depositemoneys = Depositemoney.all
    respond_with(@depositemoneys)
  end

  def show
    respond_with(@depositemoney)
  end

  def new
    @depositemoney = Depositemoney.new
    respond_with(@depositemoney)
  end

  def edit
  end

  def create

       if depositemoney_params.present? and 
              @depositemoney = Depositemoney.new(depositemoney_params)
              @depositemoney.save
              @user = User.find current_user.id
              @balance = @user.balance.to_i
              @deposite_money = depositemoney_params[:balance].to_i
              total_deposite = @deposite_money + @balance

              if total_deposite > 0
                abc = @user.update_attribute(:balance, total_deposite) 
              end  
              redirect_to profile_index_path
       end
  end

  def update
    @depositemoney.update(depositemoney_params)
    respond_with(@depositemoney)
  end

  def destroy
    @depositemoney.destroy
    respond_with(@depositemoney)
  end

  private
    def set_depositemoney
      @depositemoney = Depositemoney.find(params[:id])
    end

    def depositemoney_params
      params.require(:depositemoney).permit(:balance, :u_id)
    end
end
